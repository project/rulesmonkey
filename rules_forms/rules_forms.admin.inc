<?php


/**
 * @file
 * Implements forms events management screen.
 */
 
/**
 * Defines the forms events settings form
 */
function rules_forms_admin_events(&$form_state) {
  $form = array();
  $form_events = variable_get('rules_forms_events', array());
  $form['enable_form_id_message'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable form ID event activation message'),
    '#default_value' => $_SESSION['rules_forms_message'],
    '#description' => t('If checked, there will be a message on each form containing the form ID and a link to enable events for the form. Only visible for this session.'),
  );
  if (!empty($form_events)) {
    $form['form_events'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Form IDs that will provide events'),
      '#default_value' => array_keys($form_events),
      '#options' => $form_events,
      '#description' => t('Currently activated form IDs that will fire events for usage with Rules.'),
    );
  }
  else {
    drupal_set_message('Enable the form activation messages below and go to the form you would like to activate for Rules.', 'status', FALSE);
  }
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  return $form;
}

/**
 * Submit handler for updating rules forms settings.
 */
function rules_forms_admin_events_submit($form_id, $form_values) {
  $form_events_changed = $form_values['values']['form_events'];
  if (!empty($form_events_changed)) {
    $form_events = variable_get('rules_forms_events', array());
    // check events that have been unset
    $form_events_remaining = array_filter($form_events_changed);
    $form_events = array_intersect_key($form_events, $form_events_remaining);
    variable_set('rules_forms_events', $form_events);
  }
  $_SESSION['rules_forms_message'] = (bool) $form_values['values']['enable_form_id_message'];
  drupal_set_message(t('The settings have been saved.'));
}

/**
 * Activation page for a form ID.
 */
function rules_forms_activate($form_id_activate) {
  $form_events = variable_get('rules_forms_events', array());
  if (isset($form_events[$form_id_activate])) {
    return t('Events for %form_id have already been activated.', array('%form_id' => $form_id_activate));
  }
  return drupal_get_form('rules_forms_activate_form', $form_id_activate);
}

/**
 * Confirmation form to activate events on a form.
 */
function rules_forms_activate_form(&$form_state, $form_id_activate) {
  $default_form_label = drupal_ucfirst(str_replace('_', ' ', $form_id_activate));
  $form = array(
    'form_id_label' => array(
        '#type' => 'textfield',
        '#title' => t('Custom form label'),
        '#default_value' => $default_form_label,
        '#required' => TRUE,
        '#description' => t('Enter a custom label to better recognize the form in the administration user interface.'),
    ),
  );
  $form_state['form_id_activate'] = $form_id_activate;

  $path = array();
  $path['path'] = isset($_GET['destination']) ? $_GET['destination'] : RULES_ADMIN_FORMS_PATH;

  $title = t('Are you sure you want to activate events for %form?', array('%form' => $form_id_activate));
  $msg = t('Once the activation is confirmed, events on this form can be used to trigger rules.');
  return confirm_form($form, $title, $path, $msg, t('Activate'), t('Cancel'));
}

/**
 * Submit handler for activation of a form.
 */
function rules_forms_activate_form_submit($form, &$form_state) {
  $form_events = variable_get('rules_forms_events', array());
  $form_events[$form_state['form_id_activate']] = $form_state['values']['form_id_label'];
  variable_set('rules_forms_events', $form_events);
  drupal_set_message(t("%form has been activated.", array('%form' => $form_state['form_id_activate'])));
  $form_state['redirect'] = RULES_ADMIN_FORMS_PATH;
}